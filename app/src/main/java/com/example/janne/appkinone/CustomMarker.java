package com.example.janne.appkinone;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Represents a single note.
 *
 */
public class CustomMarker {

  long id;
  double lat;
  double lng;

  private CustomMarker() {}

  public CustomMarker(final double lat, final double lng) {
    this.lat = lat;
    this.lng = lng;
  }

  public void save(DatabaseHelper dbHelper) {
    final ContentValues values = new ContentValues();
    values.put(LAT, this.lat);
    values.put(LNG, this.lng);

    final SQLiteDatabase db = dbHelper.getReadableDatabase();
    this.id = db.insert(CUSTOMMARKERS_TABLE_NAME, null, values);
    db.close();
  }

  public static CustomMarker[] getAll(final DatabaseHelper dbHelper) {
    final List<CustomMarker> customMarkers = new ArrayList<CustomMarker>();
    final SQLiteDatabase db = dbHelper.getWritableDatabase();
    final Cursor c = db.query(CUSTOMMARKERS_TABLE_NAME,
            new String[] { ID, LAT, LNG}, null, null, null, null, null);
    // make sure you start from the first item
    c.moveToFirst();
    while (!c.isAfterLast()) {
      final CustomMarker note = cursorToMarker(c);
      customMarkers.add(note);
      c.moveToNext();
    }
    // Make sure to close the cursor
    c.close();
    return customMarkers.toArray(new CustomMarker[customMarkers.size()]);
  }

  public static CustomMarker cursorToMarker(Cursor c) {
    final CustomMarker customMarker = new CustomMarker();
    customMarker.setLat(c.getDouble(c.getColumnIndex(LAT)));
    customMarker.setLng(c.getDouble(c.getColumnIndex(LNG)));
    return customMarker;
  }


  public double getLat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public double getLng() {
    return lng;
  }

  public void setLng(double lng) {
    this.lng = lng;
  }

  public String toString() {
    return this.lat + " " + this.lng;
  }



  public static final String CUSTOMMARKERS_TABLE_NAME = "notes";
  // column names
  static final String ID = "id"; //
  static final String LAT = "latitude";
  static final String LNG = "longitude";
  // SQL statement to create our table
  public static final String CUSTOMMARKERS_CREATE_TABLE = "CREATE TABLE " + CustomMarker.CUSTOMMARKERS_TABLE_NAME + " ("
          + CustomMarker.ID + " INTEGER PRIMARY KEY,"
          + CustomMarker.LAT + " DOUBLE(4,24),"
          + CustomMarker.LNG + " DOUBLE(4,24)"
          + ");";


}