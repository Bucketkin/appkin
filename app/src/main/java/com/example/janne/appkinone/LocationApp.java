package com.example.janne.appkinone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;



public class LocationApp extends Activity implements LocationListener {
  private GoogleMap map;

  private Marker me;
  private DatabaseHelper dbHelper;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.maplocation);
    dbHelper = new DatabaseHelper(this);

    map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
            .getMap();

    map.setOnMapClickListener(new GoogleMap.OnMapClickListener(){
        @Override
        public void onMapClick(LatLng latlng) {
            saveMarker(latlng);
        }
    });

    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    // Define the criteria how to select the location provider -> use
    // default
    Criteria criteria = new Criteria();
    String provider = locationManager.getBestProvider(criteria, false);
    Location location = locationManager.getLastKnownLocation(provider);

    //request updates from gps every set seconds
    locationManager.requestLocationUpdates(provider, 10000, 0, this);

    // Initialize the location fields
    if (location != null) {
      System.out.println("Provider " + provider + " has been selected.");
      onLocationChanged(location);
    }

    if(map!=null) {

      CustomMarker[] markers = CustomMarker.getAll(dbHelper);

      int i;
      for(i = 0; i<markers.length; i++){
        CustomMarker m = markers[i];
        LatLng mLatLng = new LatLng(m.getLat(), m.getLng());
        map.addMarker(new MarkerOptions()
                .title("From db")
                .snippet(mLatLng.toString())
                .icon(BitmapDescriptorFactory.fromAsset("marker-db.png"))
                .position(mLatLng)
        );
      }
    }
  }

  @Override
   public void onLocationChanged(Location location) {

    if (map!=null){

      double lat =  (location.getLatitude());
      double lng =  (location.getLongitude());

      LatLng ME = new LatLng(lat, lng);

      if(me==null) {
        me = map.addMarker(new MarkerOptions()
                .position(ME)
                .title("Me")
                .snippet(ME.toString())
                .icon(BitmapDescriptorFactory.fromAsset("marker.png")));
          map.animateCamera(CameraUpdateFactory.newLatLngZoom(ME,12));
      } else {
        me.setPosition(ME);
        map.animateCamera(CameraUpdateFactory.newLatLng(ME));
      }
    }
  }

  @Override
  public void onStatusChanged(String s, int i, Bundle bundle) {

  }

  @Override
  public void onProviderEnabled(String s) {

  }

  @Override
  public void onProviderDisabled(String s) {

  }

//if "yes" on save marker, adds marker
  public void saveMarker(LatLng latlng) {
      final LatLng mapClicked = latlng;
      AlertDialog.Builder confirm = new AlertDialog.Builder(this);
      confirm.setCancelable(true);
      confirm.setMessage("Save location?");

      confirm.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
              map.addMarker(new MarkerOptions()
                              .position(new LatLng(mapClicked.latitude, mapClicked.longitude))
                              .title("Just saved")
                              .snippet(mapClicked.toString())
                              .icon(BitmapDescriptorFactory.fromAsset("marker-saved.png"))
              );

              new CustomMarker(mapClicked.latitude, mapClicked.longitude).save(dbHelper);
          }
      });

      confirm.setNegativeButton("No", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {}
      });

      confirm.create().show();
  }
}
